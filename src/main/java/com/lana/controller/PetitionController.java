package com.lana.controller;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lana.cache.PetitionSignatureCache;
import com.lana.model.Petition;

@RestController   					// Indicates that this class is a REST controller.
@RequestMapping("/petitions")		// Maps this controller to the "/petitions" URL path.
public class PetitionController {

	private PetitionSignatureCache cache = new PetitionSignatureCache();

    //change it to get the list from the Cache class

	@GetMapping("/all") // Maps this method to handle GET requests at the "/petitions/all" URL path.
	public List<Petition> getAllPetitions() {
		return cache.getAllPetitions(); // Returns the list of Petition objects, which will be serialized to JSON.
	}

	@PostMapping("/create")
	public Petition createPetition(@RequestBody Petition newPetition) {

		cache.addPetition(newPetition);

		return newPetition;
	}
	
}
